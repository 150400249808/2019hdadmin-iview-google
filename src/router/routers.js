import Home from '@/views/pages/home'
import demoRouter from './group/demo'
import googleRouter from './group/google'
import managementRouter from './group/management'

const base = [
  {
    path: '/',
    name: 'home',
    component: Home,
    children: [
      {
        path: '/index',
        name: 'index',
        component: () => import('@/views/pages/index')
      },
      {
        path: '/account',
        name: 'account',
        component: () => import('@/views/pages/weixin/account')
      },
      {
        path: '/pages/auto-table/:center/:model/:action',
        name: 'hdTable',
        // component: () => import('@/views/demo/hd-table')
        component: () => import('@/views/pages/auto-table')
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/pages/login')
  },
  {
    path: '/search/view-page',
    name: 'viewPage',
    component: () => import(/* webpackChunkName: "about" */ '@/views/pages/google/view-page')
  }
]
base[0].children.push.apply(base[0].children, demoRouter)
base[0].children.push.apply(base[0].children, googleRouter)
base[0].children.push.apply(base[0].children, managementRouter)
export default base
