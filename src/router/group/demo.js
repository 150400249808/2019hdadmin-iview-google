export default [
  {
    path: '/demo/tree-filter',
    name: 'treeFilter',
    component: () => import('@/views/demo/test')
  },
  {
    path: '/demo/bread',
    name: 'bread',
    component: () => import('@/views/demo/append-bread-crumb')
  },
  {
    path: '/demo/other',
    name: 'other',
    component: () => import('@/views/demo/go-other')
  }, {
    path: '/demo/test1',
    name: 'test1',
    component: () => import('@/views/demo/test1')
  }, {
    path: '/demo/test2',
    name: 'test2',
    component: () => import('@/views/demo/test2')
  }, {
    path: '/demo/test3',
    name: 'test3',
    component: () => import('@/views/demo/test3')
  }, {
    path: '/demo/test',
    name: 'test',
    component: () => import('@/views/demo/test')
  }
]
