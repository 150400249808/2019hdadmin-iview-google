export default [
  {
    path: '/google/template',
    name: 'googleTemplate',
    meta: {
      name: '模版管理'
    },
    component: () => import('@/views/pages/google/template')
  },
  {
    path: '/google/user-search',
    meta: {
      name: '用户查询'
    },
    name: 'googleUserSearch',
    component: () => import('@/views/pages/google/user-search')
  }, {
    path: '/google/user-search1',
    meta: {
      name: '用户查询'
    },
    name: 'googleUserSearch1',
    component: () => import('@/views/pages/google/user-search1')
  }, {
    path: '/google/pay',
    meta: {
      name: '用户查询'
    },
    name: 'googlePay',
    component: () => import('@/views/pages/google/pay-page')
  }, {
    path: '/google/order/:model/:action',
    meta: {
      name: '订单管理'
    },
    name: 'googlePay',
    component: () => import('@/views/pages/google/order')
  }, {
    path: '/google/searchLog/:model/:action',
    meta: {
      name: '历史查询'
    },
    name: 'googlePay',
    component: () => import('@/views/pages/google/search-log')
  }, {
    path: '/google/combo/:model/:action',
    meta: {
      name: '套餐管理'
    },
    name: 'googleCombo',
    component: () => import('@/views/pages/google/combo')
  }
]
