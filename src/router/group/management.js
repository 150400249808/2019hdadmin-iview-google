export default [
  {
    path: '/management/account/:model/:action',
    name: 'account-management',
    meta: {
      name: '账号管理'
    },
    component: () => import('@/views/pages/management/account')
  },
  {
    path: '/management/order',
    name: 'order-management',
    meta: {
      name: '订单管理'
    },
    component: () => import('@/views/pages/management/order')
  },
  {
    path: '/management/code/:model/:action',
    name: 'code-management',
    meta: {
      name: '验证码管理'
    },
    component: () => import('@/views/pages/management/code')
  }
]
