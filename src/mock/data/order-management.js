export default {
  errcode: 0,
  data: {
    pageNo: 1,
    pageSize: 10,
    results: [{
      id: '001',
      order_no: '001',
      user_id: 'test001',
      money: '100.00',
      type: '下单成功',
      pay_time: '2019-10-23 11:10:28',
      combo_id: '10001',
      create_time: '2019-10-23 11:10:28'
    }, {
      id: '002',
      order_no: '002',
      user_id: 'test002',
      money: '23.00',
      type: '支付成功',
      pay_time: '2019-10-23 11:17:11',
      combo_id: '10002',
      create_time: '2019-10-23 11:17:11'
    }, {
      id: '003',
      order_no: '003',
      user_id: 'test003',
      money: '788.00',
      type: '下单成功',
      pay_time: '2019-10-23 11:23:20',
      combo_id: '10003',
      create_time: '2019-10-23 11:23:20'
    }, {
      id: '004',
      order_no: '004',
      user_id: 'test004',
      money: '654.00',
      type: '支付成功',
      pay_time: '2019-10-23 11:18:12',
      combo_id: '10004',
      create_time: '2019-10-23 11:18:12'
    }, {
      id: '005',
      order_no: '005',
      user_id: 'test005',
      money: '342.00',
      type: '支付成功',
      pay_time: '2019-10-23 11:30:45',
      combo_id: '10005',
      create_time: '2019-10-23 11:30:45'
    }],
    totalPage: 1,
    totalRecord: 5
  }
}
