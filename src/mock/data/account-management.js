export default {
  errcode: 0,
  data: {
    pageNo: 1,
    pageSize: 10,
    results: [{
      id: '001',
      login_name: 'test001',
      password: '000000',
      company_name: '沈阳智云科技',
      industry: '互联网',
      address: '沈阳',
      main_product: '云计算',
      create_time: '2019-10-23 11:10:28'
    }, {
      id: '002',
      login_name: 'test002',
      password: '000000',
      company_name: '沈阳振兴科技',
      industry: '互联网',
      address: '沈阳',
      main_product: '大数据',
      create_time: '2019-10-23 11:17:11'
    }, {
      id: '003',
      login_name: 'test003',
      password: '000000',
      company_name: '沈阳服务科技',
      industry: '互联网',
      address: '沈阳',
      main_product: '智能硬件',
      create_time: '2019-10-23 11:23:20'
    }, {
      id: '004',
      login_name: 'test004',
      password: '000000',
      company_name: '沈阳商务科技',
      industry: '互联网',
      address: '沈阳',
      main_product: '云计算',
      create_time: '2019-10-23 11:18:12'
    }, {
      id: '005',
      login_name: 'test005',
      password: '000000',
      company_name: '沈阳云鼎科技',
      industry: '互联网',
      address: '沈阳',
      main_product: '在线教育',
      create_time: '2019-10-23 11:30:45'
    }],
    totalPage: 1,
    totalRecord: 5
  }
}
