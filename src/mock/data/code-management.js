export default {
  errcode: 0,
  data: {
    pageNo: 1,
    pageSize: 10,
    results: [{
      id: '001',
      content: '您的验证码为980627,30分钟内有效',
      mobile: '13998272839',
      create_time: '2019-10-23 11:10:28'
    }, {
      id: '002',
      content: '您的验证码为828389,30分钟内有效',
      mobile: '17856296738',
      create_time: '2019-10-23 11:10:28'
    }, {
      id: '003',
      content: '您的验证码为783873,30分钟内有效',
      mobile: '18872957382',
      create_time: '2019-10-23 11:10:28'
    }, {
      id: '004',
      content: '您的验证码为103837,30分钟内有效',
      mobile: '13382857483',
      create_time: '2019-10-23 11:10:28'
    }, {
      id: '005',
      content: '您的验证码为828647,30分钟内有效',
      mobile: '16692793678',
      create_time: '2019-10-23 11:10:28'
    }],
    totalPage: 1,
    totalRecord: 5
  }
}
