export default {
  errcode: 200,
  errmsg: 'ok',
  data: [
    {
      id: 10010,
      title: '谷歌搜索',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '模版管理', path: '/google/template', id: 1001011, icon: 'ios-keypad' },
        { title: '用户查询', path: '/google/user-search', id: 1001012, icon: 'ios-keypad' }
      ]
    },
    {
      id: 1001,
      title: '微信管理',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '用户管理', path: '/account', id: 100101, icon: 'ios-keypad' },
        { title: '素材管理1', path: '/demo/test1', id: 100102, icon: 'ios-keypad' },
        { title: '菜单管理2', path: '/demo/test2', id: 100103, icon: 'ios-keypad' },
        { title: '模版管理',
          path: 'account2',
          id: 100104,
          icon: 'ios-keypad',
          children: [
            { title: '用户管理3', path: '/demo/test3', id: 100105, icon: 'ios-keypad', permission: 'qq,bb,dd,aa' },
            { title: '素材管理0', path: '/demo/test', id: 100106, icon: 'ios-keypad' },
            { title: '菜单管理2',
              path: 'account5',
              id: 100107,
              icon: 'ios-keypad',
              children: [
                { title: '用户管理', path: 'account6', id: 100108, icon: 'ios-keypad' },
                { title: '素材管理', path: 'account7', id: 100109, icon: 'ios-keypad' },
                { title: '菜单管理2', path: 'account8', id: 100110, icon: 'ios-keypad' }
              ]
            }
          ]
        }
      ]
    },
    {
      id: 2001,
      title: '系统设置',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '用户管理', path: 'account21', id: 20012, icon: 'ios-keypad' },
        { title: '素材管理', path: 'account22', id: 20013, icon: 'ios-keypad' },
        { title: '菜单管理', path: 'account23', id: 20014, icon: 'ios-keypad' },
        { title: '模版管理', path: 'account24', id: 2005, icon: 'ios-keypad' }
      ]
    },
    {
      id: 3001,
      title: 'demo菜单',
      path: '',
      icon: 'ios-keypad',
      children: [
        { title: '面包屑导航追加',
          path: '/demo/bread',
          id: 300101,
          icon: 'ios-keypad'
        }, { title: '拖拽功能',
          path: '/demo/drag',
          id: 300102,
          icon: 'ios-keypad'
        }
      ]
    },
    {
      id: 112,
      title: '只有自己',
      path: '/demo/ziji',
      icon: 'ios-keypad'
    }
  ]
}
