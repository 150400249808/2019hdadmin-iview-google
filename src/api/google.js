import iViewTheme from 'iview-layout-themes'

const axios = iViewTheme.util.apiRequest

export const templateAction = (param, type) => {
  let url = '/wl/google/template'
  if (type === 'delete') {
    url = url + '/2'
  }
  return axios.request({
    url: url,
    type: type,
    data: param,
    php: true
  })
}

export const createRand = (param) => {
  let url = '/wl/google/createRand'
  return axios.request({
    url: url,
    type: 'post',
    data: param,
    php: true
  })
}
/**
 * 配合自动生成的资源
 * @param param
 * @param type
 * @param model
 * @param action
 * @returns {*}
 */
export const resourceAction = (param, type, model, action) => {
  let url = '/wl/' + model + '/' + action
  if (type === 'delete') {
    url = url + '/2'
  }
  return axios.request({
    url: url,
    type: type,
    data: param,
    php: true
  })
}

/**
 * 添加查询记录
 * @param order
 * @returns {*}
 */
export const addSearchLog = (data) => {
  return axios.request({
    url: 'wl/google/addSearchLog',
    type: 'post',
    data: data,
    php: true
  })
}
/**
 * 用户修改密码
 * @param data
 * @returns {*}
 */
export const userChangePwd = (data) => {
  return axios.request({
    url: 'wl/user/userChangePwd',
    type: 'post',
    data: data,
    php: true
  })
}
export const getCountChild = (data) => {
  return axios.request({
    url: 'wl/user/getCountChild',
    type: 'get',
    php: true
  })
}
