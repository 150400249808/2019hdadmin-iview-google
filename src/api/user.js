import iViewTheme from 'iview-layout-themes'

const axios = iViewTheme.util.apiRequest

/*
 管理端登录接口
 */
export const login = ({ userName, password }) => {
  const data = JSON.stringify({
    username: userName,
    password: password
  })
  return axios.request({
    url: '/api/uc/sc/loginService/userLogin',
    data: data,
    type: 'post'
  })
}
/**
 * 注册的时候检查是否存在相同的账号
 * @param account
 * @returns {*}
 */
export const checkAcount = (account) => {
  return axios.request({
    url: 'wl/user/checkAccount',
    type: 'get',
    data: { username: account }
  })
}
/**
 * 注册用户
 * @param account
 * @returns {*}
 */
export const registerUser = (data) => {
  return axios.request({
    url: 'wl/user/register',
    type: 'post',
    data: data,
    php: true
  })
}
/**
 * 修改密码
 * @param data
 * @returns {*}
 */
export const changePwd = (data) => {
  return axios.request({
    url: 'wl/user/changePwd',
    type: 'post',
    data: data,
    php: true
  })
}
export const getUserInfo = () => {
  return axios.request({
    url: '/api/as/info',
    type: 'post',
    data: { a: 1 }
  })
}
export const getJavaMenus = (menu_id) => {
  return axios.request({
    url: '/api/ac/sc/menuService/getVueMenuList',
    data: { menu_id: menu_id },
    type: 'get'
  })
}
export const getPhpMenus = () => {
  return axios.request({
    url: '/wl/menus',
    type: 'get',
    php: true
  })
}
/**
 * 发送验证码
 * @param mobile
 * @param code
 * @returns {*}
 */
export const sendMsg = (mobile, code) => {
  return axios.request({
    url: 'wl/user/sendMsg',
    type: 'get',
    data: { mobile: mobile, code: code }
  })
}
/**
 * 下单接口
 * @param combo_id
 * @param type
 * @returns {*}
 */
export const userCreateOrder = (data) => {
  return axios.request({
    url: 'wl/user/userCreateOrder',
    type: 'get',
    data: data
  })
}
/**
 * 判断是否支付成功
 * @param order
 * @returns {*}
 */
export const checkOrderStatus = (order) => {
  return axios.request({
    url: 'wl/user/checkOrderStatus',
    type: 'get',
    data: { order: order }
  })
}
/**
 * 判断用户是否支付过
 * @returns {*}
 */
export const getUserBuyList = () => {
  return axios.request({
    url: 'wl/user/getUserBuyList',
    type: 'get',
    php: true
  })
}

/**
 * 获取套餐列表
 * @returns {*}
 */
export const getComboList = () => {
  return axios.request({
    url: 'wl/user/getComboList',
    type: 'get',
    php: true
  })
}
