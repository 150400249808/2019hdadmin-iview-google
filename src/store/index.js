import Vue from 'vue'
import Vuex from 'vuex'

import iViewTheme from 'iview-layout-themes'
import createPersistedState from 'vuex-persistedstate'
import other from './model/other'

const app = iViewTheme.util.app
const user = iViewTheme.util.user
Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState({
    storage: window.sessionStorage
  })],
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    user,
    app,
    other
  }
})
