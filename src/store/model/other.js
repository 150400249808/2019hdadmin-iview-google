export default {
  state: {
    openIfreamUrl: ''
  },
  getters: {
  },
  mutations: {
    setOpenIfreamUrl (state, data) {
      state.openIfreamUrl = data
    }
  },
  actions: {
    setOpenIfreamUrl ({ commit, rootState }, obj) {
      commit('setOpenIfreamUrl', obj)
    }
  }
}
