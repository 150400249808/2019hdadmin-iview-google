import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import config from '@/config'
import iView from 'iview'
import iViewThemes from 'iview-layout-themes'
import 'iview/dist/styles/iview.css'
import 'iview-layout-themes/lib/iview-layout-themes.css'
import '@/assets/iconfont/iconfont.css'

if (process.env.NODE_ENV !== 'production') require('@/mock')
Vue.use(iView)
Vue.use(iViewThemes)

Vue.config.productionTip = false
Vue.prototype.$config = config

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
